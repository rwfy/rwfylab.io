---
author: Constantin Seibt
date: 2022-04-14
description: "Russian President Vladimir Putin has achieved at least one goal: a new world order. The battle of the systems is back. And it's time to choose the camp."
keywords: Ukraine, Putin, war, invasion
title: Russian warship, go fuck yourself!
---

**Russian President Vladimir Putin has achieved at least one goal: a new world order. The battle of the systems is back. And it's time to choose the camp.**

::: credits
By [**Constantin Seibt**](https://www.republik.ch/~cseibt){target="_blank" rel="author noopener"} (text) and **Hal Hefner** (illustration), April 14, 2022.\
Unauthorized translation from the German [**Republik** article](https://www.republik.ch/2022/04/14/russisches-kriegsschiff-fick-dich).
:::

On the morning of the invasion, a warship appeared in front of the Ukrainian Snake Island. When asked to surrender, the hopelessly outnumbered 13 border guards consulted for a few seconds. [And replied](https://news.sky.com/video/ukrainian-border-guards-tell-russian-warship-go-f-yourself-12551537), "Russian warship, go fuck yourself!"

If not everything deceives, this was a sentence for the history books. Because those 13 border guards didn't remain the only ones who said it. Shortly thereafter, the entire free world repeated it.

Long days followed, during which almost exclusively improbable things happened. No one would have believed, for example, [that the EU could act decisively](https://www.republik.ch/2022/03/16/die-rueckkehr-der-politik), that the Ukrainian army would defend Kiev, and that Russia would turn into a giant North Korea.

The world was spinning, as a hundred years earlier Lenin is said to have said according to legend: "There are decades when nothing happens -- and weeks when decades happen."

The spooky thing was the clarity of it. What had been unthinkable the day before seemed almost impossible to do otherwise the day after. It was like waking up into a nightmare.

Or as if puzzle pieces were raining down into a picture of their own accord.

There is really only one explanation for this: It was as if one had not seen beforehand what had been going on for a long time: The war has been smoldering for years, not only in Ukraine.

Now it has broken out. And we are back in the 20th century. Back in the battle of systems: Democracy or dictatorship.

It's time again to choose one's camp: Freedom or fascism.

# Putin, the bungler

Until last month, Vladimir Putin was seen as a cold, unscrupulous strategist. The latter was also morally important for him. After all, victory in the power game is the only justification for despots: All misdeeds are thereby transformed into realism.

But there is no longer any question of this. Even thinking hard, one cannot think of any politician who, after years of preparation, achieved the exact opposite of his plans:

-   Putin's main war goal was to wipe Ukraine off the map. Instead, he bombed it into a symbol of courage and freedom. No matter the outcome of the war, Ukraine will never disappear. Nothing unifies more than a just fight. And nothing is as immortal as a symbol.
-   The secondary goal was to humiliate Europe and show its weakness. But the miracle happened: the EU reached an agreement. And very quickly. And on the most consistent sanctions in its history.
-   And in the long term? [Germany is rearming after decades of restraint](https://www.sueddeutsche.de/meinung/raketenschutzschild-arrow-3-deutschland-israel-1.5555883?reduced=true). And it's quite possible that the EU will stop being a purely economic project -- and see itself as a military and political power. To this end, the previously tired NATO is back -- and for the first time, [Sweden and Finland are thinking about joining](https://time.com/6158727/finland-sweden-wrestle-with-the-benefits-joining-nato/).
-   Instead, the weakness of an institution that the whole world had feared was revealed. The Russian army disgraced itself. Nothing worked, convoys fell into ambushes, [Ukrainian tractors stole tanks](https://www.cbc.ca/news/world/ukraine-farmers-1.6387964). (Jokes on the net already call Ukrainian farmers the [fifth largest army in Europe](https://twitter.com/Biz_Ukraine_Mag/status/1501254688238362626?s=20&t=4YVCVKlFCLRL--_LlSFHwA)). At the time of the invasion, everything spoke of a blitzkrieg; today, no one expects Russian victory.
-   The dreaded Russian propaganda industry, built up over years, was destroyed by Ukraine on the very first night. It was as if someone had shut down the troll factories at the most important moment.
-   Russia itself has lost any chance of a future as a great power. Economically, it faces bankruptcy. Domestically, it transformed from rule by propaganda to rule by terror. The most realistic scenario under Putin is to become a colony of China.
-   For two decades, President Putin worked on his image as the guy with the melon-sized balls. Now he has been effortlessly overtaken by his Ukrainian counterpart Volodimir Selensky, who remained in Kiev despite bombs and assassination squads. While Putin is remembered as a bitter speaker at a 20-meter table.

It almost doesn't matter what the future holds. Such an embarrassment cannot be corrected. Putin's place in the history books is now clear -- the only question is whether among the bunglers or the criminals.

Perhaps it is a consolation to President Putin that his failure is not a personal one, but an occupational disease. If autocrats were wise, they would have to pray: Lord, let me die young! Because long-lived autocratic rulers end in violence, in agony, in disgrace.

# The curse of autocrats

Vladimir Putin has been in power for more than 20 years. Step by step, he has eliminated all competition: overambitious oligarchs, politicians and the military, the free press, and [now the Internet](https://www.republik.ch/2022/03/10/world-wide-njet). (And his wife hasn't been heard from in years either).

At some point, no one is left with any opposition. And even very suspicious people have no defense against non-stop praise. At some point they believe in their own superiority.

Which means that [autocrats sooner or later function like the structurally dumbest people](https://www.theatlantic.com/ideas/archive/2022/03/putin-dictator-trap-russia-ukraine/627064/). Because normally you can recognize intelligent people by their self-doubt; the stupidest by the fact that they think they are experts everywhere, because they have no idea of their own ignorance.

The man at the top suspects this -- and is ever more meticulous about loyalty. As a result, he receives increasingly absurdly doctored news. And not only he, but also his ministers. And their officials. And endlessly further down the chain of command.

Which leads to any harmless specialist with a realistic view getting in trouble for loyalty problems. In the end, large parts of the empire ruled by the strong man exist only on paper.

An empire of vacuum emerges.

# Good at violence, bad at war

The vacuum is exacerbated when the top boss is a gangster. And Putin is fabulously rich -- quite a few analysts [consider him the richest person on the planet](https://fortune.com/2022/03/02/vladimir-putin-net-worth-2022/). Officially, though, he earned [around \$140,000](https://qz.com/1594989/vladimir-putins-financial-disclosure-claims-little-wealth/) before the sanctions, at least according to one official figure from 2018; [his vacation cottage on the Black Sea is a dream of cream-colored kitsch for \$1.4 billion](https://www.insider.com/vladimir-putins-secret-palace-black-sea-russia-video-2021-1).

And even if President Putin is admirably discreet -- his confidants all became billionaires.

Now you'd think gangsters would know something about violence. It is their core business. And Putin also systematically invested [several hundred billion dollars in modernizing the Russian army](https://thediplomat.com/2015/06/putin-to-press-on-with-russias-military-modernization/).

But when he ordered them into Ukraine, the armories often contained nothing but rust and paper -- [tanks had no spare parts](https://www.csmonitor.com/World/Europe/2014/0410/Can-Russia-s-military-fly-without-Ukraine-s-parts), the [rocket launchers had brittle tires](https://twitter.com/trenttelenko/status/1499164245250002944), the [fuel was somewhere](https://www.jpost.com/international/article-698800). The troops had learned how to handle money from their commander. Most of Putin's modernization ended up in uniform pockets.

And as usual with dictators, the army bought sparkling superweapons, [but far too few boring trucks](https://www.youtube.com/watch?v=b4wRdoWpw0w). An army is first and foremost a logistics company. Without supplies, it is a colossus with stubby legs.

Putin's other problem is: with only force you gain power, but lose the war. Thus, his soldiers were greeted with anti-tank missiles all over Ukraine. At the same time -- at least in the east of the country -- a reception with flowers was expected.

With reason. Ukraine had long been deeply divided: the Ukrainian-speaking west, the Russian-speaking east. In all elections up to the Maidan revolution in 2014, the [two parts had voted for completely different parties](https://www.washingtonpost.com/news/worldviews/wp/2014/01/24/this-is-the-one-map-you-need-to-understand-ukraines-crisis/) -- pro-Europe in the west, pro-Kremlin candidates in the east. But that changed: in 2019, Selenski was [elected across the board](https://en.wikipedia.org/wiki/2019_Ukrainian_presidential_election). Why?

Because Putin had acted. In 2014, at the same time as he occupied Crimea, he also financed and [armed the rebellion](https://www.aljazeera.com/news/2022/2/4/ukraine-crisis-who-are-the-russia-backed-separatists) in two Ukrainian provinces in the Donbass. There, warlords loyal to Putin had now ruled for eight years, waging permanent war and robbing anyone who did not join their forces. In Russia, Putin could sell this as liberation -- in Ukraine, they were too close.

So it was clear to the Russian-speaking Ukrainians that Putin was not their brother, but the brother of the gangsters -- and that they had to fight like lions if their future was dear to them.

(Which turned out to be a correct assumption: The majority Russian-speaking towns are now being shot to rubble day after day by the Russian army).

# Idiots to power

But the real tragedy about an autocracy is not the billions stolen, but the lives stolen. Because society is systematically riddled with cynics, bureaucrats and idiots.

That because autocracies resemble the mafia in structure: the boss, his confidants, their confidants, then the rest of the world.

Mafia gangs are not incompetent: they are masters of sucking out business. But not building them. That's why they're active around the world, mostly in illegal industries like drug smuggling. Or if legal: in simple businesses. The classic is exporting. Those who sell stolen goods always make a margin.

But if a mafia takes over a more complex industry, such as one in a competitive market, there are only two options. It ruins the business almost immediately. Or it ceases to be a mafia.

This logic, according to historian and journalist [Kamil Galeev](https://twitter.com/kamilkazani/status/1501425939942715393?s=20&t=HqwFt1dPYq3rN5zmrh3h1g), explains the economic power structure in Putin's Russia:

-   In terms of influence, wealth, prestige, the oligarchs of the oil and gas industry are at the top. This was taken away from the old oligarchs and is now under the control of Putin's confidants. Because it accounts for the lion's share of exports -- and because selling raw materials is lucrative even without any experience: any idiot can become very rich very quickly.
-   The exploitation of gold, iron, nickel, copper, etc. requires a lot more know-how. Putin therefore left the mining rights in the hands of the oligarchs he had taken over from his predecessor in the presidency, Boris Yeltsin -- on condition that they stay out of politics.
-   Of necessity, the elite leaves complex sectors such as information technology and industry to the entrepreneurs and engineers, in short: the nerds. But they have no prestige and no lobby. (For example, an oligarch doesn't really care if the import prices for machine parts go through the roof as the ruble sinks. They want cheap machines for their corporations. And when they fail to do so, they [have pirated Czech models made](https://twitter.com/kamilkazani/status/1501369950463836164?s=20&t=HqwFt1dPYq3rN5zmrh3h1g)).

The characteristic of autocratic economics is that in practice it is synonymous with political power. And this goes in two directions:

1.  Whoever has the right connections to power gets the business.

2.  Whoever gets business gets power. That's why close attention is paid to making sure that only the right people do business. (Industry is systematically kept small by the oligarchs).

The latter is often forgotten in autocracies. It is not only true that loyalty and connections count most for a career -- and skills almost nothing. But the reverse is also true: If the goal of leadership is to maintain power, competence, efficiency and commitment are considered suspect per se. (That's also why you'll never appease a tyrannical boss by doing a good job).

In short, maintaining power in an autocratic state necessarily involves sabotaging all sectors in which a countervailing power could form.

(The principle of sabotage also applies, of course, to the oligarchs themselves: The price of remaining an oligarch is political eunuchism. Their power is also measured almost exclusively by proximity to Putin's ear. For this, they are required to be willing to finance Putin's projects: political ones like [troll factories](https://www.deutschlandfunk.de/us-justiz-klagt-russen-an-der-mann-der-putins-koch-genannt-100.html), private ones like [Putin's palace](https://time.com/5934092/navalny-putin-palace-investigation/)).

In this system of maintaining power through bondage, economic deficits are considered collateral damage at best. On average, the gross national product in a dictatorship [drops by 0.12 percentage points](https://www.sciencedirect.com/science/article/pii/S0264999321003035) for each additional year of the man in power.

In Putin's Russia, the pariah sectors are industry ([Russia has the industrialization level of an emerging market](https://www.ifri.org/sites/default/files/atoms/files/rnv96_inozemtsev_uk_ok.pdf)), provincial governments and the army.

At first glance, Russia is a military country. It has nuclear missiles, [the second largest army ever](https://www.businessinsider.in/defense/ranked-the-worlds-20-strongest-militaries/slidelist/51930339.cms#slideid=51930371) -- and the [country's official myth is based on its victory over Nazi Germany](https://twitter.com/kamilkazani/status/1509976566872293381?s=20&t=do_Csnfkv620HC-NNHypIg).

But in reality, the Russian army is socially only slightly above the cockroaches in its barracks. [It is no coincidence](https://twitter.com/kamilkazani/status/1502682176848027658?s=20&t=HqwFt1dPYq3rN5zmrh3h1g) that entire military units, even the nuclear missile silos, pay protection money to the Russian mafia -- the mafia is higher in the hierarchy. Thus, [officers are mocked on television](https://twitter.com/kamilkazani/status/1502701731746099200?s=20&t=HqwFt1dPYq3rN5zmrh3h1g), generals are liquidated if they are too popular, training is poor, so is equipment, and manners are even worse: bullying often reigns in the barracks -- and it is [not uncommon for recruits to be sold into prostitution by their officers](https://www.theguardian.com/world/2007/feb/14/russia.lukeharding). Not surprisingly, those who can shirk do so -- the majority of recruits come [from the mouse-poor provinces](https://twitter.com/kamilkazani/status/1506479259866394625?s=20&t=mcQdnk5d2oEZEvDCf2TISA) where certain ethnic groups live.

The reason is simple: Putin's power is based on the security services -- that's why the army is kept as low as possible. And non-military intelligence officers are infiltrated into the hierarchy everywhere. And otherwise, in case of doubt, the most unpopular and untalented are promoted to officer.

Putin's autocracy talks endlessly of superiority and pride; what it does is what an infestation of parasites always does: paralysis.

The color of autocracy is the mixture between cruelty and gray. A life without liveliness.

In short, President Putin was anything but a successful statesman long before the invasion of Ukraine.

But where did the worldwide cult around him come from?

# Fascism Ltd.

Why does Trump call Putin a ["genius"](https://www.politico.com/news/2022/02/23/trump-putin-ukraine-invasion-00010923), does Berlusconi call him ["the number one"](https://www.reuters.com/article/us-ukraine-crisis-berlusconi-idUSKCN0RR0MW20150927) political leader, does Roger Köppel call him the ["last realist of Europe"](https://de.rt.com/programme/fasbender/119810-fasbender-im-gesprach-mit-roger/)? Why the admiration of Matteo Salvini, Viktor Orbán, Marine Le Pen, Tucker Carlson, Jair Bolsonaro?

The answer: Putin was, at least until a few weeks ago, the ideal embodiment of a fascism, a civilized, profitable fascism, a physically and politically survivable fascism.

Because radical fascism has nothing to do with politics; there is nothing left to negotiate. Fascism is a cult -- the cult of purity and strength. But in practice it is the cult of annihilation -- what is weak must be eradicated. First the others, then oneself -- who, that is only a question of date.

No wonder, a state form of annihilation is popular only among radical suicides: it cannot survive for long.

The proto-fascist autocracy is the state form of impotence -- it produces nothing. It is far more stable because its goal is not change. Neither by concrete measures nor by revolution. It relies mainly on rhetoric. Its heroism consists in endless invocation of strength -- and in the even more endless denunciation of weakness.

Whereby strength always resides only in the glorious past and the victorious future -- as in Trump's ingenious formula "Make America Great Again!"

While in the present, the strong must struggle through the dictatorship of the weak -- through a swamp of softness, decadence, gender dissolution, censorship and morality. Which, unfortunately, means that he is constantly held back in solving the problems.

Autocracy is the omicron variant of fascism. With mitigated symptoms (instead of the destruction of the political opponent, the destruction of political discourse, instead of street fighting, cultural fighting), but all the more contagious for it. Not least because the "do-gooders prevent everything" endless loop is useful for autocratic politicians in opposition as well as in power.

Of course, one advocates cruelty. But these, if there is trouble, were simply "realism". Or, if there really is trouble, "a joke". Or if that doesn't work either, "fake news".

In short: the supporters of autocracy have invented something really useful even in functioning democracies: Fascism without liability.

# Bullshit

This is another reason why Putin is the hero: He has perfected authoritarian propaganda and financed it worldwide.

The new propaganda works very differently than it did in the 20th century, when the main goal was to convince the opponent of your own system. It is largely disconnected from any substance: a mutation that makes it extremely automatable, extremely adaptable, in short: extremely viral.

[Its recipes](https://www.rand.org/pubs/perspectives/PE198.html) are, in broad terms:

1.  It is loud. It runs on as many channels as possible -- from social media to its own news channel to studies and congresses. What counts is quantity. The more often someone hears the same statement, the more plausible it becomes.

2.  It's fast. People tend to stick to the first piece of information they hear on the subject.

3.  It is independent of facts. Forgoing research, even plausibility, is a key advantage when it comes to quantity and speed. (And you are never faster than when you have invented the event yourself.) Whereby facts are not spurned either. The only important thing is to permanently sow doubt in the audience. And to constantly engage one's enemies with one's own things.

4.  It doesn't care about consistency. Which means that one can let loose unencumbered by one's own arguments. (For example, lamenting the world conspiracy of the Jews and ten minutes later accusing someone of anti-Semitism). So that you can drive any arbitrary person before you with any arbitrary accusation.

Steve Bannon, the former Trump campaign manager, [summed up his strategy as](https://edition.cnn.com/2021/11/16/media/steve-bannon-reliable-sources/index.html) "To flood the zone with shit."

And the mayor of Kiev, Vitaly Klitschko, even condensed the formula to a single word. When a reporter asked him, amid the ruins of his city, what he thought of the Russians' insistence on not bombing civilian targets: ["Bullshit!"](https://edition.cnn.com/videos/world/2022/03/17/klitschko-ukraine-russia-war-kyiv-civilians-newday-vpx.cnn)

Indeed, bullshit has lost all innocence. Gone are the days when bullshit was a tiresome waste of time at openings and cadre meetings. Bullshit is now the sharpest weapon of the global right.

And amazingly versatile:

-   Donald Trump deserves credit for discovering that in politics bullshit works without camouflage. When criticized, he accuses the other side of double talk, just as he did on the playground. Or dryly mutter, "Fake news!" Or just talk anything. This is enormously efficient, especially for government politicians: Once you're in charge, you don't need to respond to anything.
-   Bullshit is also the raw material for the culture war -- which, instead of concrete policies, is basically about things that don't happen in the lives of the people who talk about them: Burkas, intersex toilets, gender stars, speech bans, politicians drinking children's blood, chocolate heads, illuminati, and vaccine damage.
-   Last, but not least, bullshit is also the centerpiece for the homage paid to authoritarian politicians.

The leading Swiss Putin supporter, as a politician and publicist in the party of the most powerful Swiss oligarch, Roger Köppel, wrote, for example, on the eve of the invasion:

> Putin exposes the hollow moralism of his opponents. And the decadence of the West. While our politicians are debating whether minors should be allowed to change their sex for seventy francs at the residents' registration office without their parents' consent, Putin is deploying his armored divisions. Message: There is still something like a hard reality of facts out there, not only the imaginary metaverse of 'discourses' and 'narratives', with which one arranges the world as one would like it to be. Perhaps, hopefully, Putin is the shock the West needs to come back to its senses.

::: credits
"[Little Psychology of Putin Criticism](https://weltwoche.ch/story/kleine-psychologie-der-putin-kritik/)", "Weltwoche", Feb. 23, 2022.
:::

Suppose this were part of a serious debate. What could be said in response? "Do you really believe that Mr. Putin is criticized because of his lack of wokeness and not because of the gathering of an invading army? And that what thereby bothers Putin's critics most is that it distracts from the unbureaucratic change of sex for Swiss children, and not that an invasion army is preparing an invasion?"

Even if Mr. Köppel likes to claim to enliven the debate with a different opinion -- absolutely nothing clever can be said in response to his texts.

Besides, while you're still talking, Mr. Köppel would have long since published a dozen more videos, editorials or tweets: [for example, that](https://weltwoche.ch/daily/strategie-des-westens-gescheitert-wenn-du-einen-gegner-nicht-besiegen-kannst-musst-du-ihn-einbinden-wir-sollten-die-sanktionen-gegen-russland-aufheben-und-mit-putin-an-den-verhandlungstisch-alles/) "every fifth-grader" knows that you have to "get along" with the bully on the school playground and that therefore the "wisdom of common sense" dictates that all arms deliveries and sanctions against Russia should be stopped. And: that in view of the historically oblivious warmongering of the competition, he felt himself to be [one of the last flawless pacifists](https://twitter.com/KoeppelRoger/status/1500148295397740549?s=20&t=LgH4iMmlnJYYrCi6QB3Z4g). Et cetera.

What can be said about this?

Nothing, really. They are simply words that fall out of Roger Köppel's mouth, they could be others, it doesn't even matter.

There isn't even anyone speaking to that. None of these above thoughts is personal. It is pre-cut industrial goods, a set of formulations that are manually reassembled around the planet by politicians and publicists.

For example, at about the same minute that Köppel was writing his Putin essay, Steve Bannon was producing [a radio show on the same topic](https://twitter.com/RonFilipkowski/status/1496645647386263553?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1496645647386263553%7Ctwgr%5E%7Ctwcon%5Es1_c10&ref_url=https%3A%2F%2Fwww.newsweek.com%2Fformer-trump-adviser-steve-bannon-backs-anti-woke-vladimir-putin-1682122). The guest was Erik Prince, the founder of a notorious [mercenary firm](https://en.wikipedia.org/wiki/Blackwater_(company)):

> Bannon: "Putin is not woke. He is anti-woke."
>
> Prince: "The Russian people still know which toilet to use."
>
> Bannon: "How many genders are there in Russia?"
>
> Prince: "Two."
>
> Bannon: "They don't hang out Pride flags there ..."
>
> Prince: "Yeah, they don't have boys participating in girls' college swimming competitions."
>
> Bannon: "How backward! How savage! How medieval!"

::: credits
From: «War Room», Steve Bannon's podcast.
:::

In short, this is the standard international maelstrom that floods public debate with interchangeable bullshit.

Of course, it's no coincidence that it's mainly aged, lousy gentlemen with a passion for breaking the same taboos over and over again who ply this trade. It is intellectual impotence.

From everything they say, it follows: nothing.

But that would be thinking too kindly. It is not a matter of private decay. It is about the non-stop clogging of public discourse, about an attack on democracy.

In short: professional proto-fascist propaganda.

And that is why there is, after all, a response to texts like Roger Köppel's:

"Russian warship, go fuck yourself!"

# On the cutting edge

Almost everyone involved, [including in Ukraine](https://www.aljazeera.com/news/2022/2/21/why-ukrainians-dont-believe-in-war-with-russia-distrust-west), had thought Putin's deployment was a bluff until hours before the invasion.

The invasion came as a shock. President Putin gave [a long speech](https://www.zeit.de/politik/ausland/2022-02/wladimir-putin-rede-militaereinsatz-ukraine-wortlaut) on Russian television. Shortly thereafter, Russian tanks rolled into Ukraine from three sides.

But the response also came as a shock, very quickly, very decisively.

By the evening of the second day, the Ukrainian army had already [killed most of the Russians' advance troops](https://en.wikipedia.org/wiki/Battle_of_Antonov_Airport). It blocked the roads to Kiev, attacked the supply routes and inflicted terrible losses on the overwhelming Russian army.

Ukrainian President Volodimir Selensky exchanged his suit and tie for an olive green T-shirt and became the face of the resistance worldwide. At the latest, when the U.S. offered him the opportunity to flee, Selensky replied ([at least according to legend](https://www.washingtonpost.com/politics/2022/03/06/zelenskys-famous-quote-need-ammo-not-ride-not-easily-confirmed/)), "I need ammunition, not a ride."

Shortly thereafter, the U.S. and EU decided on far more massive sanctions than expected; a few days after the invasion, the ruble exchange rate, international banking connections, the stock market and 30 years of economic construction were ruined.

Moreover, taboos fell in rows -- Germany, traditionally Russia's closest partner in the EU, halted the Nord Stream 2 pipeline project, decided on a permanent massive arms buildup, and supplied weapons to Ukraine. [Neutral Sweden also supplied weapons](https://euobserver.com/world/154526). Over 600 international corporations [withdrew from Russia](https://www.republik.ch/2022/04/06/eine-geschichte-der-enttaeuschung). And in the end, to the amazement of the world, even Switzerland was on board.

Where did this resistance come from -- as if from nowhere?

In 2014, things were still completely different. At night and in the fog, elite soldiers of the Russian army in uniforms without insignia had occupied Crimea. Russian troops in the Donbass had slaughtered the Ukrainian army almost effortlessly, the EU and the US were at odds, and their sanctions were a sad joke.

Perhaps for that very reason. There is a truth that the country that loses a war wins in peace afterwards. It is because the winners have no reason to rethink everything, but the losers urgently do.

At least in this case they did.

Previously corrupt, rigid, poorly trained like Russia's today, the Ukrainian army was [completely reorganized in 2016](https://theconversation.com/in-2014-the-decrepit-ukrainian-army-hit-the-refresh-button-eight-years-later-its-paying-off-177881). It had been seen that the strict chain of command had shackled soldiers on the battlefield -- by the time senior officers approved a decision by lower-ranking officers, they were often dead.

So they democratized the decisions downward -- and trained (with American military specialists) the non-commissioned officers. Added to this was the good fortune of a lack of money: so no superweapons were procured, but rather unsexy material: Turkish drones and comparatively cheap portable anti-aircraft and anti-tank missiles. These now proved more effective than expected. (Also, because the Russian army never managed to deploy its various troops as an ensemble -- so small Ukrainian commandos could take them out one at a time).

In addition, the troops' officers had war experience: in eastern Ukraine, Putin-funded rebels kept an undeclared but brutal small-scale war smoldering -- [with 14,000 dead in the last eight years](https://www.crisisgroup.org/content/conflict-ukraines-donbas-visual-explainer).

Further, Putin had unified the country in 2014. First, by persuading President Viktor Yanukovych, who was loyal to Moscow, to overturn an agreement with the EU at the last minute. Whereupon students demonstrated for their future on Maidan Square in Kiev, daily, for months. Yanukovych had them beaten up with the help of Russian agents. Then their parents and grandparents joined the students. Yanukovych had them shot with live ammunition -- a few days later he was overthrown. Yanukovych fled to Moscow, according to rumors, with boxes full of dollars. Putin took advantage of the chaos and had Crimea occupied overnight. After that, the Ukrainian army could hardly save itself from volunteers.

And last but not least, Putin provided the Ukrainian army with the thrust of its first successes by committing the sin of believing in his own propaganda. Officially starting not a war but a "special operation" against the "regime of Nazis and drug addicts," he [consequently sent special police and paratroopers ahead](https://twitter.com/kamilkazani/status/1499377671855292423?s=20&t=8N6k4Q8trnqWOpiwSRTchQ). These were specialized in insurgencies, looked fearsome, but were only lightly armed, completely inexperienced in war -- and dead after a few days.

No man has done more for Ukraine's unity and the strength of its army than Vladimir Putin. Nevertheless, it would be wrong to talk mainly about him. After the initial shock, a great many people in Ukraine and in Western countries were up to speed.

# Ideas as weapons

What was really new was that the Western camp reacted to Putin's assault not only with harshness. But with ingenuity.

And not with small improvements, but with real ideas -- ideas that demand commitment and ability.

-   In Ukraine, with the reinvention of its own army as an army from below.
-   In Germany, where the new red-green liberal government has reoriented the entire security policy.
-   In the case of sanctions, where the decisive blow was a real invention: they separated the Russian central bank from international payments. It worked like a financial nuclear bomb -- overnight [Russia lost access to half of its state assets](https://www.bloomberg.com/news/articles/2022-03-13/russia-lost-access-to-half-its-reserves-finance-minister-says). This left Putin's war chest, which had been filled for years, and which was supposed to be used in the event of sanctions, worthless.
-   Russian propaganda also took such a bad grip because the U.S. was driving a radical new strategy, [publishing intelligence reports on preparations for the Russian invasion in real time for weeks](https://www.gchq.gov.uk/speech/director-gchq-global-security-amid-russia-invasion-of-ukraine): Satellite images of troop concentrations, memoranda from the Kremlin, Russian propaganda plans such as a [video shoot of professionally made-up corpses to fake a Ukrainian attack](https://www.theguardian.com/world/2022/feb/21/dumb-and-lazy-the-flawed-films-of-ukrainian-attacks-made-by-russias-fake-factory). This did not prevent the invasion, but it did take away the Russian side's chance to create confusion. When the war broke out, there was no doubt who had started it.

The decisive factor was not least the inventiveness of two presidents:

-   U.S. President Joe Biden not only realized faster than anyone that Putin was indeed planning war; but he reacted as few other American politicians would have dared: with inconspicuousness. Only after the invasion was it discovered what a job the Americans had done. The West agreed on sanctions overnight -- which would never have happened if the American Secretary of State had not been [touring Europe for weeks](https://www.state.gov/secretary-blinkens-travel-to-ukraine-germany-and-switzerland) beforehand. And as soon as the war broke out, it was learned that the Americans were negotiating [with their ex-enemies Venezuela and Iran about resuming oil deals](https://www.aljazeera.com/news/2022/3/8/venezuela-talks-us). And the devil knows how: The [supply of Ukraine with weapons went perfectly from the first moment](https://www.nytimes.com/2022/03/06/us/politics/us-ukraine-weapons.html). There's a saying in the Army, "Amateurs talk guns, professionals talk logistics." Faced with a choice between bang and effect, Joe Biden is obviously a professional.
-   Meanwhile, Ukrainian President Volodimir Selensky reinvented an archaic form of war propaganda: the propaganda of courage. In battle, the commander belongs in the lead. Perhaps it took an entertainer in the presidency to know this: how infectious courage is. For that, it doesn't matter that a story is made up. You see someone brave in a movie theater and you leave it a few inches taller. It doesn't even matter if there are hardened professional politicians in the audience. Supposedly, a video call with Selenski gave EU leaders the final push not to compromise on sanctions [when he greeted them with the words](https://www.businessinsider.com/zelensky-told-eu-leaders-this-might-be-the-last-time-you-see-me-alive-2022-2?r=US&IR=T): "Ladies and gentlemen, this may be the last chance to see me alive."

The devil knows if it will be enough. Goodness knows what's to come. But for once, the democracies of the West kept pace with the times.

But unless everything is mistaken, the first weeks were only the beginning of the horror.

# World war

Russia's President Putin ultimately has only one option left -- to wash away his disgrace with blood. By conquering the brotherly country he wanted to liberate as a graveyard.

And if that fails, to take the way out that all despots take: to perpetuate his own greatness by the scale of his failure.

So far, President Putin's actions speak for themselves: in all the small wars he started, he had entire cities razed to the ground after the initial resistance. And unfortunately, the Russian army is an artillery army. Their strength: destroying infrastructure. And kill civilians.

And they've already started: The recently thriving port city of Mariupol is a cold, bloody pile of debris; in the failed [siege of Kiev alone, 75 children died in the first month of the war](https://www.ukrinform.net/rubric-ato/3447243-158-children-killed-in-ukraine-since-largescale-russian-invasion-began.html); in the small town of Butsha, retreating Russian troops left the [streets, basements and front yards full of residents' corpses](https://www.theguardian.com/world/2022/apr/03/they-were-all-shot-russia-accused-of-war-crimes-as-bucha-reveals-horror-of-invasion).

If everything does not deceive, this is [also the targeted strategy of Russian warfare](https://www.nytimes.com/2022/03/29/opinion/ukraine-war-putin.html): the more terror it spreads, the more favorable the negotiating position -- because no one can justify with responsibility that its indiscriminate killing continues.

One should have no illusions that incompetence makes autocrats more harmless. Nothing is more dangerous than bunglers.

In the second country he is ruining -- Russia -- Putin has chosen the same path: the path of terror. In a single month an authoritarian democracy became a fully formed fascist state. Dissemination of undesirable news about the war is punishable [by up to 15 years in prison](https://www.reuters.com/world/europe/russia-introduce-jail-terms-spreading-fake-information-about-army-2022-03-04/); all media are under censorship; [the Internet has been decoupled](https://www.republik.ch/2022/03/10/world-wide-njet); people are carted off to jubilant events; [in the secret services and the army, the first officers are arrested](https://www.thetimes.co.uk/article/kremlin-arrests-fsb-chiefs-in-fallout-from-ukraine-invasion-chaos-92w0829c5); Putin himself [makes speeches](https://www.themoscowtimes.com/2022/03/16/putin-announces-measures-to-overcome-western-economic-blitzkrieg-a76968) in which he promises a "purge" and that his people will spit out traitors "like a fly that accidentally flew into their mouth".

For the time being, this threat was directed at the oligarchs who have absconded abroad -- but such threats are never limited to one group.

As for Putin's situation, it is unclear. Dictators rarely end with an announcement. They usually end when a lower-ranking part of the elite feels threatened by the man at the top. And acts.

Who that might be in Russia is unclear. Putin's personal buddies at the top of power, his intelligence chiefs, ministers and oligarchs, have obtained their position not through any skills. [But by their proximity to the president](https://www.republik.ch/2022/03/22/die-silowiki-sind-die-echte-russische-elite). They have no legitimacy without him.

And an uprising is not going to happen. [In the first month of the war, the surveyed approval rating for Putin rose from 71 to 83 percent](https://www.forbes.com/sites/dereksaul/2022/03/31/putins-domestic-approval-rating-reaches-highest-level-in-five-years/). Whatever happens: blame the West. And if not: then corrupt officials must have screwed up -- the president was deceived.

Still, Putin is up against the wall. The Russian economy is hurtling toward bankruptcy, the ruble may be headed for hyperinflation, and the Russian army is no longer worth much.

It takes little imagination to know that chaos reigns in Russia's control centers. Nothing can be relied on anymore, the argument "We do it this way ..." is dead. Standards are dead. Planning is dead. Because that is the essence of chaos, that one only reacts.

For this, guilty parties are sought everywhere. For the present as well as the future disasters. Which means: From now on, everyone fights for himself. Which also means: goods, information, money -- everything is hoarded, in the offices, in the provinces, in every household. Russia is disintegrating out of fear; and the only counterforce is terror.

([For example, sugar](https://www.newsweek.com/russian-citizens-food-shortage-ukraine-supermarket-sanctions-fight-viral-video-1688194) is already the cause of hoarding, beatings, theft and arrests).

May be that the regime will collapse overnight. But it may also be that it will go after more resources. And then we have to be prepared for something.

According to letters [from an alleged analyst in the FSB intelligence service](https://www.igorsushko.com/2022/03/putin-expects-west-to-blink-in-face-of.html), agents are working without pause on a scenario that relies on the last great resource: that the West is more afraid of war than the Russian government.

The scenario includes Russia officially treating sanctions and arms deliveries as a declaration of war. And an ultimatum is given to the West. Stop immediately, otherwise missiles would be fired at Estonia, Latvia and Lithuania -- and above all at Poland. This is because Poland is the open border to Ukraine: the refugees come out, the weapons and aid come in.

The goal in this scenario, which is considered realistic, is that the West will end up being intimidated and will leave Ukraine to the Russian grasp. After which victory there becomes possible, with puppet government, mass arrests and re-education camps.

It is quite possible that this is [just nonsense](https://twitter.com/christogrozev/status/1504724839654907931?s=20&t=ZyPGTfnoalXyrkZpsWoNgg). And the sender is a propagandist of the Russian secret service, spreading what is Putin's last great trump card: fear. Or an employee of the Ukrainian secret service who says why NATO has to intervene: out of fear. Or a paranoid spreading what he feels: fear.

If you listen to the governments, it doesn't sound much cozier. The Americans warn of chemical weapons in Ukraine and cyberattacks in the West. [While a Kremlin spokesman on CNN does not rule out the use of nuclear weapons](https://edition.cnn.com/2022/03/22/europe/amanpour-peskov-interview-ukraine-intl/index.html) if "Russia's existence" is threatened.

Whereas the most likely scenario is that of a long, cruel tearing apart in Ukraine. [After a month of painful losses, the Russian generals decided](https://www.themoscowtimes.com/2022/03/26/russia-signals-less-ambitious-goals-in-ukraine-war-a77091) that the war's goal had never been regime change in Kiev, but to eliminate the Ukrainian army from the start. Now Russia is massing troops in resource-rich eastern Ukraine.

It will still take some nerve, poise, clarity and luck. Because it is absolutely unpredictable what will happen. This is also because Vladimir Putin, at least in the past, always chose a single strategy: escalate. As absurd as it is, World War III has actually become a conceivable variant for 2022.

Who knows, if you are man and woman, you will have this dialogue in some bunker:

**You as a man**: "Unbelievable that now the whole planet will be destroyed just because one guy didn't get what he wanted ..."

**You as a woman**: "Welcome to my everyday life."

And then that's the last thing you say.

(This scene was actually written here only because women are otherwise completely absent from this story).

# The club of autocrats

In early February 2022, three weeks before the invasion, [Russia and China signed an agreement of "joint borderless friendship"](https://www.reuters.com/world/europe/russia-china-tell-nato-stop-expansion-moscow-backs-beijing-taiwan-2022-02-04/). That was on the eve of the opening ceremony of the Winter Olympics in Beijing -- and [as one could read later](https://www.nytimes.com/2022/03/02/us/politics/russia-ukraine-china.html), Russia's President Putin promised Chinese party leader Xi to wait until after the closing ceremony to invade Ukraine.

The meeting was a sign of the self-confidence of authoritarian politics. At least since the financial crisis of 2008, China and Russia have increasingly clearly seen themselves as models for the future. Since then, their propaganda has repeated: democracies are morally decadent and politically weak. They can't get their stuff together anymore. We will win!

There are reasons for this confidence: In recent years, more and more democracies have toppled.

Because a lot of democracies are split near the middle. Elections are thus a dance on the razor's edge. A few measly votes -- and the country tilts in a completely different direction. Trump beat Clinton in crucial swing states by about a hundred thousand votes, as Biden later beat Trump. [The British voted 52-48 percent in favor of Brexit](https://www.bbc.co.uk/news/politics/eu_referendum/results). (In Switzerland, the mass immigration initiative won [by 50.3 percent](https://swissvotes.ch/vote/580.00)).

The next round comes in France. President Emmanuel Macron won the first round of voting against Putin ally Marine Le Pen; [according to current polls](https://www.leparisien.fr/elections/presidentielle/sondage-presidentielle-emmanuel-macron-legerement-favori-face-a-marine-le-pen-au-second-tour-10-04-2022-S7XBNDAJLNFPRIO3V452YBQEXA.php), he will win the runoff with 54 to 46 percent. The [*Economist* prediction model](https://www.economist.com/interactive/france-2022/forecast) gives him an 80 percent chance of winning the election. Which is like the democracy in the center of Europe playing Roussian Roulette (Just with one and a half cartridges in the revolver).

With a determined person at the helm, the authoritarian camp needs only one victory. Many authoritarian heads of state have been democratically elected -- the first time. For Vladimir Putin, that first election victory was enough to establish stable rule; others, like Donald Trump, failed narrowly.

Whereby stable authoritarian rule is an illusion. It develops in spurts, usually when one's power meets resistance. Trump tried a revolt when he lost re-election. Putin began to see himself as Russia's spiritual savior [when there were demonstrations against election fraud throughout the country in 2011 to 2013](https://en.wikipedia.org/wiki/2011%E2%80%932013_Russian_protests). (He effectively changed status from elected to chosen.) Viktor Orbán [was voted out in 2002 as a devout liberal](https://www.dw.com/de/regierungswechsel-in-ungarn/a-502952) -- in the second election in 2010, he [won as a herald of illiberal democracy](https://www.zeit.de/politik/ausland/2010-04/ungarn-wahl).

The thrusts always go in the same direction: fascism. Faster with each push.

In her major essay "The Bad Guys Are Winning," [Anne Applebaum described](https://www.theatlantic.com/magazine/archive/2021/12/the-autocrats-are-winning/620526/), among other things, the development of Belarusian President Alexander Lukashenko, who was in office for 26 years. After [his sixth election in 2020](https://www.latimes.com/world-nation/story/2020-08-10/belarus-leader-alexander-lukashenko-wins-sixth-term), hundreds of thousands demonstrated, day after day. Lukashenko first let demonstrators talk, then beat them up, but the protests continued to grow. Until Lukashenko's rival Putin sent him a plane with FSB secret agents. The regime then changed tactics: the wild mass arrests stopped, and instead police made targeted nighttime visits -- Putin's specialists knew that eliminating individual key figures in the protest was enough for the rest to lapse into apathy.

Lukashenko now crossed one red line after another. For a long time, opposition figures had simply been put in prison. Now came torture. A few months later came the rapes. Then came the murders. First at home, then abroad. And finally, the air force [forced down a commercial airliner](https://www.nytimes.com/2021/05/23/world/europe/ryanair-belarus.html) on its overflight towards Lithuania, in which an opposition blogger was sitting.

Putin basically delivered to Lukashenko the same service he had delivered to Syrian dictator Assad a few years earlier.

The alternative would have been the Chinese package:

1.  Recognize all of China's domestic claims.
2.  Buy Chinese surveillance technology, run by a crew of Chinese specialists.
3.  Accept Chinese investment -- and we'll make you personally rich, too.
4.  Lean back.

The motive for such assistance services was a double shock: in 1989, after the fall of the Berlin Wall, then in 2010 during the Arab Spring, dictators fell like dominoes. This made their surviving colleagues realize:

1.  Courage and rebellion are contagious.

2.  Rebellion, no matter where, must be nipped in the bud.

3.  Solidarity across all borders is worthwhile.

In fact, ideology plays almost no role under authoritarian regimes. Whatever the official propaganda, in practice there is now an Authoritarian International.

Lukashenko may now be an outcast in the free world. But that does not mean he is isolated. He has no problem [with China's communists](https://www.mfa.gov.cn/ce/ceus//eng/zgyw/t1658999.htm), [Saudi sheikhs](https://www.reuters.com/article/us-belarus-russia-idUSKBN1ZN130), the [military junta in Burma](https://eng.belta.by/politics/view/ways-to-intensify-belarus-myanmar-relations-discussed-in-minsk-141205-2021/), the [clerics in Iran](https://www.tehrantimes.com/news/465172/Raisi-meets-Lukashenko-says-his-priority-is-economy), the Taliban, [socialist Venezuela](https://eng.belta.by/president/view/lukashenko-plans-to-meet-with-venezuelan-president-soon-143934-2021/).

In the 20th century it was different: The Politburo of the Soviet Union and a fascist dictator like Pinochet in Chile still considered their own ideology and their good reputation in the world worth awkward contortions -- and they would have arrested anyone who declared them to be people of the same ilk.

Today, there is no shame. Under authoritarian regimes, image is as unimportant as the government's record. No one raises an eyebrow when you've ruined your country, as long as you retain power. Maduro rules a slum in Venezula, Assad rules a slaughterhouse in Syria.

And it's no problem in the authoritarian camp if you commit genocide -- like the junta in Burma. And if there is protest, it doesn't matter how broad, peaceful, globally noticed that protest is. What counts is that it is put down, in Minsk as well as in Hong Kong.

Justifications are no longer needed. Usually an expletive is enough, such as: "Imperialists!" Putin tried hard. He even found two in the attack on Ukraine: "Nazis and drug addicts".

That's how it works in the club.

# The attack in the West

Democracy in Ukraine was far from perfect. Neither was its president Selensky -- five months before the invasion it [was revealed](https://www.bpb.de/themen/europa/ukraine/342240/dokumentation-offshore-geschaefte-selenskyj-und-kolomojskyj-in-den-pandora-papers/) that he had opened various offshore accounts.

Nevertheless, Ukrainians did not hesitate for a second to defend their imperfect democracy against the second largest army on the planet. They had a reason to put their lives on the line: They still knew from experience what it means when a country joins the club.

Further west, they were luckier. So much so that it was forgotten that they had ever needed any.

After the fall of the Wall, in the 1990s, theory won out, [that it could not have come otherwise](https://podcasts.apple.com/au/podcast/timothy-snyder-on-the-myths-that-blinded-the-west/id1548604447?i=1000554066715): that democracy and free markets were not the result of a long, dangerous struggle, but of a natural evolution.

As with any ideology, the penalty was blindness. The authoritarian currents, parties, states were thought to be retards. And their propaganda was not taken seriously. Neither their declaration of war later.

The invasion of Ukraine suddenly made it clear: fascism is back. And it is everywhere. Authoritarian governments are not only in Russia. They are in China and Pakistan, Brazil and Bolivia, Iran and Saudi Arabia, India and North Korea, Syria and Egypt -- as well as in Hungary, Serbia and Poland, right in the middle of the EU.

In general, there are few democratic countries that are free of cancer: Authoritarian parties are rampant across Europe. And not just parties: During the pandemic, spontaneous movements marched all over Europe, their hard core staying only briefly with the medical debate. And then quickly tilted: towards radical politics (overthrow dictatorship!), mistrust (politics, science, press -- all bought!), reality distortion (the QAnon crap), Darwinism (contamination separates the strong from the weak), sadism (all vaccinated die in two weeks -- good thing!), whining (we live like the Jews under the Nazis), megalomania (the Federal Council must let us govern). The only consistency here: manners from hell.

No question, there are honorable reasons not to get vaccinated. But not a single reason to behave this way. The loud part of the protests was a sauce of esoterics, embittered people and the right-wing radicals, who in their turn were almost forced to march along because their topics were already on the posters without them.

It is no coincidence that far-right politicians, conspiracy supporters, Corona skeptics and the Putin worshippers are now growing together. For example, after the invasion of Ukraine, [40 percent of SVP sympathizers showed understanding for Putin](https://www.tagesanzeiger.ch/die-svp-und-ihr-problem-mit-wladimir-putin-575325312570). And [in Canada](https://www.thestar.com/news/investigations/2022/03/19/how-vaccination-status-might-predict-views-on-the-russian-invasion-of-ukraine.html), 26 percent of the unvaccinated thought the Russian invasion was justified, but only 2 percent of the vaccinated.

The authoritarian bloc is forming. And not by coincidence, it looks the same almost everywhere in the world.

For years, Russia and China have built their network undisturbed. China takes its influence mainly through hard currency. Around the world, the Chinese are buying infrastructure: ports, airports, telecommunications, real estate, key industries (like the seed producer Syngenta in Switzerland), plus they are building a gigantic new Silk Road through Asia. So that one day they won't even have to blackmail the rest of the world, because everyone knows that without Beijing, the lights go out.

Russia took the cheaper route by funding an army of trolls to flood the world's democracies with doubt and bullshit. While China constricted the world through constructivism, Russia managed a marvel of destructiveness -- the most successful sabotage machine in history. The nonstop stream of noise, venom and sheer mischief damages the efficiency of the countries under attack through distrust, friction, waste of time.

Plus two triumphs that the Soviet Union could only have dreamed of. Without the attack of Russian propaganda there would be in all probability: no Brexit, no President Donald Trump.

Not without reason Putin lost almost all respect for his western antagonists. The two most powerful Anglo-Saxon countries -- paralyzed by a ridiculously cheap investment in bullshit.

And the devil knows if this investment will not bring him victory -- in the American elections of 2024. Because the Republicans are already a fascist party. For Trump, they are ready for anything -- even to justify storming Congress. (Thus, virtually no Republican finds anything wrong anymore [when the wife of one of the seven chief justices of the U.S. confers with Trump's chief of staff](https://edition.cnn.com/2022/03/25/politics/ginni-thomas-clarence-thomas-supreme-court-ethics/index.html) about invalidating the presidential election, arresting the elected Democratic politicians and putting them before a military tribunal).

The U.S. Republican Party is by far the greatest threat to civilization in the 21st century -- if Trump won in 2024, he would not wait until two weeks before the end of his presidency to stage a coup.

The horrifying thing is that the party is rotten to the bone. Say Trump loses in 2024, does his copy win in 2028? Or not until 2032?

Barring a miracle, America will be in the fascist camp in the near future.

Which in turn means that there is no need to fear the outbreak of World War III. It has been going on for a long time.

And the end is anything but certain: authoritarian regimes rule about half of humanity, they have support from powerful groups in democracies, they have a propaganda structure and strategy, and they have a common goal: to topple one country after another.

It is time to end our own blindness. We cannot afford any more losses.

Ukraine is not just about Ukraine. It is about everything.

# After getting up

One encouragement is that you are not alone in your incompetence. Ukrainian President Volodimir Selensky was awakened at 5 a.m. at home during the first missile strikes on February 24. [He later said](https://www.economist.com/europe/2022/03/27/volodymyr-zelensky-in-his-own-words), "None of us was ready for the war before it began."

The president adhered to a simple rule: "As my father said, if you don't have a clue what to do, be honest and you'll be fine. Because if you're honest, people will believe you. Try not to pretend."

And after all, so far, neither Selenski nor his country has done badly.

Sincerity is not the worst idea. Because in the unknown, you have no other compass. And we have awakened in an unknown world: without neutrality between good and evil. There is an incredible amount to think about, to throw overboard, to do.

A few tasks are obvious:

-   To deliver money and weapons to Ukraine: if only because this country also defends our freedom.
-   To research in the financial center where Russia remains vulnerable. Plus a quick, perhaps immediate, decoupling from Russian gas.
-   On the above, not to wait. As Selenski rightly noted, "Now we are hearing that the decision depends on whether Russia launches a chemical attack on us. This is not the right approach. We are not guinea pigs."

The rest gets a bit more complex: Such as the transformation into a political project and the rearmament of the EU; an economy designed less for efficiency and more for resilience; an energy policy that makes it independent of the oil and gas autocracies; the question of how democrats work together in democracy; the most effective strategy against the submarine parties of authoritarians; crippling the troll factories and building counter-propaganda in autocratic countries; better nerves in the face of nuclear bomb threats; ending the habit of responding to kicks in the shins by handing over a business card.

In short: all the things for which the current recipe books are empty.

# The return of politics

That is why the epoch of our youth has just come to an end. With Putin's invasion, the last of the three great promises of the nineties was also broken:

1.  The free market determines rational prices → The crash of the international banking system in 2008.
2.  Change through trade: open markets create open societies → The giant markets of China and Russia export autocracy.
3.  Globalization eliminates war: bombing trading partners is bad business → Russia's invasion.

In retrospect, neoliberalism seems almost touching: as the return of the romantic idea of "back to nature". Only this time the dream was expressed in economic models: that paradise would return if only man did not intervene.

The banking crash, the Corona pandemic, the Authoritarian International showed that there is no natural law that leads to the maximization of reason and harmony. Instead, the message was that there was no longer an excuse to avoid politics. Because without a framework, everything falls apart: freedom, society, even banks.

In addition, the engine of the thirty-year ultraliberal epoch is just breaking down -- globalization. This, because it showed how vulnerable complex supply chains make the world.

1.  During the pandemic: when borders close -- and entire industries come to a standstill without supplies.
2.  Through the force of sanctions: China and co. will try to reroute their financial and goods flows past the US.
3.  By Russia's war of aggression: the West is trying to make itself independent of the raw materials and investments of its enemies.

In all likelihood, production will become more local, more expensive, more national: because in a world divided into camps, efficiency is no longer the main goal, but resilience.

It is, as Olivia Kühni wrote in her essay, time for [the return of politics](https://www.republik.ch/2022/03/16/die-rueckkehr-der-politik).

Thus, at the grave of neoliberalism, two decidedly political economic systems are fighting to take over the inheritance: Big Brother and Big Government.

-   Big Brother: Whoever has political power gets access to the economy. And makes himself and his followers rich.
-   Big Government: Whoever has the political power distributes as much of the capital as possible to projects and the population.

It is no coincidence that a global minimum tax has emerged in recent years, just like Corona aid packages, and that three tax relief bills have been rejected in Switzerland. (And no coincidence that both Biden and Scholz campaigned on the slogan "respect".)

That because for many democracies, the sense of inequality has become a real danger. If a large enough portion feels insufficiently respected, all it takes is one election and the country tilts.

After all, this is not the first time we have been on this cliff.

# Battle of the systems

In fact, freedom and capitalism have won a few times against authoritarian systems -- how?

By doing what authoritarian systems can't: reinventing themselves.

The liberal revolution in Switzerland achieved its lonely victory in the 19th century. It succeeded not in the short Sonderbund War, but in the turmoil that followed. When the victorious liberals, at a horrendous pace, stamped out the constitution, codes, courts, elementary schools, universities, currency, railroads, banks, a little later the people's rights, in short: an entire country from the green meadow.

It was a bow to man by deed.

In the U.S., [the response of Franklin D. Roosevelt](https://www.republik.ch/2021/02/20/theorie-und-praxis-der-politischen-furchtlosigkeit) to the depression of the 1930s was "nothing to fear but fear itself" -- and experimentation: a mess of several dozen infrastructure programs, smashing of monopolies, high top taxes, higher wages -- this was the cradle of the Western middle class. And Roosevelt's response to Nazism was a shameless reach into the legal bag of tricks: a deal to "loan" Britain (later also the Soviet Union) thousands of shiploads of war materiel -- an outrage that probably decided the world war. (U.S. propaganda christened the armories "conveyor belts of democracy".)

The young middle class, with high wages and high taxes, also won the next battle on the civilian conveyor belts: the Cold War against the Soviet empire.

Capitalism convinced not so much by theory of history, but by: Televisions, washing machines, tower hairdos, cigarettes, color movies, existentialism, swimming pools, rock 'n' roll, single-family homes, comic books, rock concerts, beach vacations, revolts and automobiles.

So through a plethora of squeaky-clean stuff, sold with squeaky-clean promises. That there was a lot of junk among them was not so bad: the crucial thing was that capitalism had evolved, from supply to demand, from the cool gaze of the factory owner to the dreamy gaze of the salesman: it understood man as a creature of desire.

In short, the democracies of the past, surrounded by enemies, came up with something: a liberal state, the New Deal, the social market economy. They provided their citizens with more participation. And more dignity.

(In German, dignity is not without reason also a subjunctive: dignity consists in having possibilities).

Democratic renewal continues to function as a combat strategy to this day: How susceptible autocracies are to it was already seen in the first month of the war.

-   With a new philosophy, the Ukrainian army took the Russian war machine by surprise: With the Ukrainians, small units decide on the field, while the Russians follow the chain of command.
-   To this end, the attackers were rudely surprised to find that in Ukraine, individual mayors and governors decided for themselves -- so the war plan to topple the government in Kiev was old news even before the first shot was fired.
-   Likewise, Moscow underestimated the force of the resistance: They didn't think Biden, the EU, Selensky could come up with anything energetic. (Cynics anticipate everything except that someone has a heart).

Without that, the Russian flag would now be flying over Kiev. Because when both sides play by the same rules, the authoritarians win -- because they don't play by them.

It is not for nothing that the worship of strong men includes the cult of cruelty. On the grounds that they can still make tough decisions. And in contrast to the effeminate democracies, they can still make sacrifices. Which is also true: Principles and people, including their own, are play material for them.

But the great strength of democracies is precisely this: They can neither be indifferent to the rules nor to the people. No democracy functions without the rule of law -- and no democracy survives without dignity: without the possibility for its citizens to make something of their lives.

Selenski, for instance, [knows this](https://twitter.com/tomiahonen/status/1509672105612386307?s=20&t=e0isGtSVbNA1QISSJilR-g): "Whatever happens, we all have to think about the future, about what Ukraine will be like after this war, what our lives will be like because this is a fight for our future." He's right: if you're fighting people like Putin, you have to think about the future.

Because the greatest danger threatens democracies not from outside, but from within: When distrust and political noise have crippled them and a [candidate like Trump asks](https://www.youtube.com/watch?v=t-jasg-_E5M), "What do you have to lose?"

The decision in the battle of the systems will not be made on the battlefield, but in politics. More precisely, in the question of whether the democracies still have the strength and the imagination to manage another great feat.

# Our job

And -- are we creating something new?

Just a few weeks ago, one would have said: unthinkable. But since then, many unthinkable things have happened. The United States was forward-looking. Europe united. And the Ukrainians had the heart, the skill and the intelligence to run down the Russian military machine.

It's time to throw out some of our own opinions, listen, make alliances, and learn quickly. Because fascism is back. And it wants a revenge. Nobody knows yet whether the coming war will be hot or cold. The only thing that is clear is that we will win, because otherwise there will be nothing left to make life worth living. Or to put it more elegantly: Russian warship, go fuck yourself!

All this is nothing new. Generations have fought for the democracies we grew up in. Now it's our turn.
