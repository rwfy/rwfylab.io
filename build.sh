#!/bin/bash

# build HTML
if command -v pandoc >/dev/null ; then
  pandoc --from=markdown \
         --to=html5 \
         --template=pandoc_template.html5 \
         --css=water.dark.min.css \
         --include-in-header=in-header.html \
         --include-before-body=before-body.html \
         --standalone \
         --columns=9999 \
         --output=public/index.html \
         source.md
else
  echo "\`pandoc\` not found. Get it here: https://pandoc.org/installing.html"
  exit 1
fi

# tweak external links
if command -v sd >/dev/null ; then
  sd '(<a href="[^"]+")>' '$1 target="_blank" rel="noopener">'  public/index.html
fi

# copy CSS
cp water.dark.css public/water.dark.min.css

# minify HTML and CSS
if command -v minify >/dev/null ; then
  minify --recursive \
         --output=. \
         public/
else
  echo "\`minify\` not found. Get it here: https://github.com/tdewolff/minify/tree/master/cmd/minify"
  exit 1
fi
